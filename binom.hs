binom :: Int -> Int -> Int
binom n k = function n k `div` function k k

function:: Int -> Int -> Int
function n k = f n k 1
	where f n k s
		| k == 0	= s
		| otherwise = f n (k - 1) s * (n - k + 1)
		

main = print (binom 7 12)