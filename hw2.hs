akkerman :: Int -> Int -> Int
akkerman m n = 
	if m == 0
		then n + 1
		else if m > 0 && n == 0
			then akkerman (m - 1) 1
			else if m > 0 && n > 0
				then akkerman (m - 1) (akkerman m (n-1))
				else 0



factorial :: Int -> Int
factorial 0 = 1
factorial x = x * (factorial (x-1))

fibonachi :: Int -> Int
fibonachi 0 = 0
fibonachi 1 = 1
fibonachi n = (fibonachi (n-1)) + (fibonachi (n-2))


main = do
	print (factorial 6)
	print (akkerman 2 4)
	print (fibonachi 7)