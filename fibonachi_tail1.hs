fibonachi :: Int -> Int
fibonachi n = fibonachi_tail n 0 1 0

fibonachi_tail :: Int -> Int-> Int -> Int -> Int
fibonachi_tail n k x y=
        if n == k    
        	then y
        	else fibonachi_tail n (k + 1) (x + y) x

main = print(fibonachi 7)
	