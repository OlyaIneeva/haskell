module BinaryTree where

data Tree x = Empty
				| Node (Tree x) x (Tree x)
				
height :: Tree x -> Integer
height Empty = 0
height (Node left _ right) = (max (height left) (height right) + 1)


treeMap :: Tree x -> (x -> y) -> Tree y
treeMap Empty f = Empty
treeMap (Node left z right) f = Node (treeMap left f)
									 (f z)
									 (treeMap right f)

