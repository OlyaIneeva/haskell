module Oatmeal where


type OatmealTemp = Int

lowest_t, perfect_t, highest_t :: OatmealTemp

lowest_t  = 20
perfect_t = 50
highest_t = 100


data Adjustment = TurnLeft | NoAction | TurnRight
								deriving (Show)


	
oatmeal_temp_to_adjustment :: OatmealTemp -> Adjustment
oatmeal_temp_to_adjustment t
	| t >= 20 && t <= 50    = func t


func :: OatmealTemp -> Adjustment
func t
	| t < perfect_t  = TurnRight
	| t == perfect_t = NoAction
	| t > perfect_t  = TurnLeft
	
main = print (func 30)