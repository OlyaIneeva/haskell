module YesItCanFlyAirlines where

data DinnerOrder = Chicken | Pasta | NoMeal deriving(Show)


dinner_order_to_msg :: DinnerOrder -> String
dinner_order_to_msg m = case m of 
	Chicken -> ("The passenger ordered " ++ show m)
	Pasta 	-> ("The passenger ordered " ++ show m)
	NoMeal 	-> "The passenger didn't order any meal."
main = print (dinner_order_to_msg Pasta)