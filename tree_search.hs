module BinarySearchTree where

data Tree = Empty | Node Tree Integer Tree
	    deriving (Show, Read, Eq)




testTree = Node Empty 4 Empty
treeElem :: Integer -> Tree  -> Bool
treeElem x Empty = False
treeElem x (Node left a right)
     | x == a = True
     | x < a  = treeElem x left
     | x > a  = treeElem x right



heightTree :: Tree -> Integer
heightTree Empty = 0
heightTree (Node l _ r) = (max (heightTree l) (heightTree r)) + 1

elemSum :: Tree -> Integer
elemSum Empty = 0
elemSum (Node left value right) = (elemSum left) + (elemSum right) + value
main = do 
		print (treeElem 4 testTree)
		print (heightTree testTree)
		print (elemSum testTree)

