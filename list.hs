
data List x y = Empty | Pair x y (List x y) 
									deriving (Show)

lengthL :: List x y -> Integer
lengthL Empty = 0
lengthL (Pair x y next) = (lengthL next) + 2

dMap :: List x y -> (x -> z) -> (y -> a) -> List z a
dMap Empty f g = Empty
dMap (Pair x y next) f g = Pair (f x)
								(g y)
								(dMap next f g) 

main = print (dMap (Pair "qwe" 2 (Pair "asd" 3 Empty)) (\a -> a ++ " changed") (\b -> b + 1))