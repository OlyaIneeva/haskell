map_foldl :: (a -> b) -> [a] -> [b]
map_foldl f(x:xs) = foldl (\ys y -> (f y):ys) [f x] xs

map_foldr :: (a -> b) -> [a] -> [b]
map_foldr f(x:xs) = foldr (\x xs -> (f x):xs) [] xs

main = do
	print $ map_foldl (\x -> x*x) [1, 2, 3]
	print $ map_foldr (\x -> x*x) [1, 2, 3]