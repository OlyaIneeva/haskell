filter_recursive :: (a -> Bool) -> [a] -> [a]
filter_recursive _ [] = []
filter_recursive f (x:xs) = 
				if (f x) == True
					then x:(filter_recursive f xs)
					else filter_recursive f xs

concat_recursive :: [[a]] -> [a]
concat_recursive [] = []
concat_recursive (x:xs) = x ++ (concat_recursive xs)


concat_map_recursive :: (a -> [b]) -> [a] -> [b]
concat_map_recursive _ [] = []
concat_map_recursive f (x:xs) = (f x) ++ (concat_map_recursive f xs)

