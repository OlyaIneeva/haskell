filter_foldl :: (y -> Bool) -> [y] -> [y]
filter_foldl f(x:xs) = foldl(\xs x -> if (f x) then xs ++ [x] else xs) [] xs

filter_foldr :: (y -> Bool) -> [y] -> [y]
filter_foldr f(x:xs) = foldr(\x xs -> if (f x) then x:xs else xs) [] xs

concat_foldl :: [[y]] -> [y]
concat_foldl(x:xs) = foldl(\xs x -> xs ++ x) [] xs

concat_foldr :: [[y]] -> [y]
concat_foldr(x:xs) = foldr(\x xs -> x ++ xs) [] xs

concat_map_foldl :: (y -> [z]) -> [y] -> [z]
concat_map_foldl f(x:xs) = foldl(\xs x -> xs ++ (f x)) [] xs

concat_map_foldr :: (y -> [z]) -> [y] -> [z]
concat_map_foldr f(x:xs) = foldr(\x xs -> (f x) ++ xs) [] xs